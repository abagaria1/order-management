package com.falabella.OrderManagementDemo.service;

import com.falabella.OrderManagementDemo.exception.InventoryException;
import com.falabella.OrderManagementDemo.models.Inventory;
import com.falabella.OrderManagementDemo.repository.InventoryRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
class InventoryServiceImplTest {

    @Mock
    private InventoryRepository inventoryRepository;

    @InjectMocks
    private InventoryServiceImpl inventoryService;

    @Test
    void printWelcome(){
        String result = inventoryService.welcomeToFalabella();
        assertEquals("Welcome to Order Management Service !!!!", result);
    }

    @Test
    void fetchInventoryList_success_inventoryService() throws InventoryException {
        Inventory inventory = new Inventory(1001L,250,"Ranchi");
        List<Inventory> inventoryList = new ArrayList<>(List.of(inventory));
        when(inventoryRepository.findAll()).thenReturn(inventoryList);

        List<Inventory> inventories = inventoryService.fetchInventoryList();
        assertThat(inventories).isNotNull();
        assertThat(inventories.size()).isEqualTo(1);
    }

    @Test
    void fetchInventoryList_failure_inventoryService(){
        List<Inventory> inventoryList = new ArrayList<>();
        when(inventoryRepository.findAll()).thenReturn(inventoryList);
        assertThrows(InventoryException.class, () -> {
            inventoryService.fetchInventoryList();
        });
        verify(inventoryRepository, times(1)).findAll();
    }

    @Test
    void saveInventory_success_inventoryService() throws InventoryException {
        Inventory inventory = new Inventory(1001L,250,"Ranchi");
        when(inventoryRepository.findById(inventory.getId())).thenReturn(Optional.empty());
        when(inventoryRepository.save(inventory)).thenReturn(inventory);

        Inventory savedInventory = inventoryService.saveInventory(inventory);
        assertThat(savedInventory).isNotNull();
    }

    @Test
    void saveInventory_failure_inventoryService(){
        Inventory inventory = new Inventory(1001L,250,"Ranchi");
        when(inventoryRepository.findById(inventory.getId())).thenReturn(Optional.of(inventory));

        assertThrows(InventoryException.class, () -> {
            inventoryService.saveInventory(inventory);
        });

        verify(inventoryRepository, never()).save(inventory);
    }

    @Test
    void updateInventory_success_inventoryService() throws InventoryException {
        Inventory inventory = new Inventory(1001L, 250, "Ranchi");
        when(inventoryRepository.findById(inventory.getId())).thenReturn(Optional.of(inventory));
        inventory.setQuantity(350);
        inventory.setLocation("Raipur");
        when(inventoryRepository.save(inventory)).thenReturn(inventory);
        Optional<Inventory> updatedInventory = inventoryService.updateInventory(inventory, inventory.getId());
        assertThat(updatedInventory.get().getQuantity()).isEqualTo(350);
        assertThat(updatedInventory.get().getLocation()).isEqualTo("Raipur");
    }

    @Test
    void updateInventory_failure_inventoryService(){
        Inventory inventory = new Inventory(1001L, 250, "Ranchi");
        when(inventoryRepository.findById(inventory.getId())).thenReturn(Optional.empty());
        assertThrows(InventoryException.class, () -> {
           inventoryService.updateInventory(inventory, inventory.getId());
        });
        verify(inventoryRepository, never()).save(inventory);
    }

    @Test
    void deleteInventory_success_inventoryService() throws InventoryException {
        Inventory inventory = new Inventory(1001L, 250, "Ranchi");
        when(inventoryRepository.findById(inventory.getId())).thenReturn(Optional.of(inventory));

        willDoNothing().given(inventoryRepository).deleteById(inventory.getId());
        inventoryService.deleteInventoryById(inventory.getId());

        verify(inventoryRepository, times(1)).deleteById(inventory.getId());
    }

    @Test
    void deleteInventory_failure_inventoryService() {
        Inventory inventory = new Inventory(1001L, 250, "Ranchi");
        when(inventoryRepository.findById(inventory.getId())).thenReturn(Optional.empty());
        assertThrows(InventoryException.class, () -> {
           inventoryService.deleteInventoryById(inventory.getId());
        });
        verify(inventoryRepository, never()).deleteById(inventory.getId());
    }

    @Test
    void updateInventoryForDemand_failure_demandGreater_inventoryService(){
        Inventory inventory = new Inventory(1001L, 250, "Ranchi");
        when(inventoryRepository.findById(inventory.getId())).thenReturn(Optional.of(inventory));

        assertThrows(InventoryException.class, () -> {
            inventoryService.updateInventoryForDemand(Optional.of(inventory), 350);
        });
        verify(inventoryRepository, never()).save(inventory);
    }

    @Test
    void updateInventoryForDemand_success_demandGreater_inventoryService() throws InventoryException {
        Inventory inventory = new Inventory(1001L, 250, "Ranchi");
        when(inventoryRepository.findById(inventory.getId())).thenReturn(Optional.of(inventory));
        inventory.setQuantity(250);
        when(inventoryRepository.save(inventory)).thenReturn(inventory);
        Optional<Inventory> updatedInventory = inventoryService.updateInventoryForDemand(Optional.of(inventory), 200);
        System.out.println(updatedInventory.toString());
        assertEquals(updatedInventory.get().getQuantity(), 50);
    }

}