package com.falabella.OrderManagementDemo.service;

import com.falabella.OrderManagementDemo.exception.InventoryException;
import com.falabella.OrderManagementDemo.models.Demand;
import com.falabella.OrderManagementDemo.models.Inventory;
import com.falabella.OrderManagementDemo.repository.DemandRepository;
import com.falabella.OrderManagementDemo.repository.InventoryRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class DemandServiceImplTest {

    @Mock
    private DemandRepository demandRepository;

    @Mock
    private InventoryRepository inventoryRepository;

    @InjectMocks
    private DemandServiceImpl demandService;

    @Mock
    private InventoryServiceImpl inventoryService;

    @Test
    public void fetchDemandList_success_demandService() throws InventoryException {
        Demand demand = new Demand(101L, 1001L, 250, "Ranchi");
        List<Demand> demandList = new ArrayList<>(List.of(demand));
        when(demandRepository.findAll()).thenReturn(demandList);

        List<Demand> demands = demandService.fetchAllDemands();
        assertThat(demands).isNotNull();
        assertThat(demandList.size()).isEqualTo(1);
    }

    @Test
    public void fetchDemandList_failure_demandService(){
        List<Demand> demandList = new ArrayList<>();
        when(demandRepository.findAll()).thenReturn(demandList);

        assertThrows(InventoryException.class, () -> {
           demandService.fetchAllDemands();
        });

        verify(demandRepository, times(1)).findAll();
    }

    @Test
    public void saveDemand_failure_inventoryNotPersisted_demandService(){
        Demand demand = new Demand(101L, 1001L, 200, "Ranchi");
        Inventory inventory = new Inventory(1001L,250,"Ranchi");
        when(inventoryRepository.findById(inventory.getId())).thenReturn(Optional.empty());

        assertThrows(InventoryException.class, () -> {
            demandService.saveDemand(demand);
        });

        verify(demandRepository, never()).save(demand);
    }

    @Test
    public void saveDemand_failure_demandGreater_demandService(){
        Demand demand = new Demand(101L, 1001L, 300, "Ranchi");
        Inventory inventory = new Inventory(1001L,250,"Ranchi");

        assertThrows(InventoryException.class, () -> {
            inventoryService.updateInventoryForDemand(Optional.of(inventory), demand.getQuantity());
            demandService.saveDemand(demand);
        });

        verify(demandRepository, never()).save(demand);
    }

    @Test
    public void saveDemand_success_demandService() throws InventoryException {
        Demand demand = new Demand(101L, 1001L, 200, "Ranchi");
        Inventory inventory = new Inventory(1001L,250,"Ranchi");
        when(inventoryRepository.findById(demand.getProductId())).thenReturn(Optional.of(inventory));
        inventory.setQuantity(250);
        Optional<Inventory> updatedInventory = inventoryService.updateInventoryForDemand(Optional.of(inventory), demand.getQuantity());
        when(demandRepository.save(demand)).thenReturn(demand);
        Optional<Demand> saveDemand = demandService.saveDemand(demand);
        assertEquals(demand.getQuantity(), 200);
    }

}