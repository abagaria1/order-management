package com.falabella.OrderManagementDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class OrderManagementDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderManagementDemoApplication.class, args);
	}

}
