package com.falabella.OrderManagementDemo.service;

import com.falabella.OrderManagementDemo.exception.InventoryException;
import com.falabella.OrderManagementDemo.models.Demand;

import java.util.List;
import java.util.Optional;

public interface DemandService {

    Optional<Demand> saveDemand(Demand demand) throws InventoryException;
    List<Demand> fetchAllDemands() throws InventoryException;
}