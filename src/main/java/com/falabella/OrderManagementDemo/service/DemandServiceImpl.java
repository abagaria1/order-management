package com.falabella.OrderManagementDemo.service;

import com.falabella.OrderManagementDemo.exception.InventoryException;
import com.falabella.OrderManagementDemo.repository.InventoryRepository;
import com.falabella.OrderManagementDemo.models.Demand;
import com.falabella.OrderManagementDemo.models.Inventory;
import com.sun.istack.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.falabella.OrderManagementDemo.repository.DemandRepository;

import java.util.List;
import java.util.Optional;

@Component
public class DemandServiceImpl implements DemandService{

    @Autowired
    private DemandRepository demandRepository;

    @Autowired
    private InventoryRepository inventoryRepository;

    @Autowired
    private InventoryServiceImpl inventoryService;

    private final Logger logger = (Logger) LoggerFactory.getLogger(DemandServiceImpl.class);

    @Override
    public Optional<Demand> saveDemand(@NotNull Demand demand) throws InventoryException {
        Optional<Inventory> inventory = inventoryRepository.findById(demand.getProductId());
        if(inventory.isEmpty()) {
            logger.info("========== Inventory is not present with Id : " + demand.getProductId() + " Demand cannot be processed ==========");
            throw new InventoryException("Inventory for the given product Id : " + demand.getProductId() + " does not exist!!!");
        }
        inventory = inventoryService.updateInventoryForDemand(inventory, demand.getQuantity());
        logger.info("========== Inventory Updated with given Product Id : -> " + demand.getProductId() + " ==========");
        return Optional.of(demandRepository.save(demand));
    }

    @Override
    public List<Demand> fetchAllDemands() throws InventoryException {
        List<Demand> demandList = (List<Demand>) demandRepository.findAll();
        if(demandList.isEmpty()){
            logger.info("There are no demands to be shown. Please try again after Posting a Demand !!!");
            throw new InventoryException("There are no demands to be shown. Please try again after Posting a Demand !!!");
        }
        logger.info("========== Fetching all the demands from Database ==========");
        return demandList;
    }
}
