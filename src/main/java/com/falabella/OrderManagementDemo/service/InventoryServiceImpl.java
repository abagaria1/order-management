package com.falabella.OrderManagementDemo.service;

import com.falabella.OrderManagementDemo.exception.InventoryException;
import com.falabella.OrderManagementDemo.models.Inventory;
import com.sun.istack.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import com.falabella.OrderManagementDemo.repository.InventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

@Service
public class InventoryServiceImpl implements InventoryService {

    @Autowired
    private InventoryRepository inventoryRepository;

    private final Logger logger = LoggerFactory.getLogger(InventoryServiceImpl.class);

    @Override
    @CacheEvict(value = "inventories_cache", allEntries = true)
    public Inventory saveInventory(Inventory inventory) throws InventoryException {
        Optional<Inventory> inventoryPersisted = inventoryRepository.findById(inventory.getId());
        if (inventoryPersisted.isPresent()) {
            logger.info("========== Inventory persisted for Id : " + inventory.getId() + " and thus cannot ReSave ==========");
            throw new InventoryException("Inventory already present with the given Id");
        }
        logger.info("========== Saving inventory with Id : " + inventory.getId() + " ==========");
        return inventoryRepository.save(inventory);
    }

    @Override
    @Cacheable("inventories_cache")
    public List<Inventory> fetchInventoryList() throws InventoryException {
        List<Inventory> inventoryList = (List<Inventory>) inventoryRepository.findAll();
        if(inventoryList.isEmpty()){
            logger.info("There are no products in the inventory. Please add a product to view here !!!");
            throw new InventoryException("There are no products in the inventory. Please add a product to view here !!!");
        }
        logger.info("========== Finding all the records for Inventory ==========");
        return inventoryList;
    }

    @Override
    @CacheEvict(value = "inventories_cache", allEntries = true)
    public Optional<Inventory> updateInventory(@NotNull Inventory inventory, Long inventoryId) throws InventoryException {
        Optional<Inventory> inventoryPersisted = inventoryRepository.findById(inventoryId);
        if(inventoryPersisted.isEmpty()) {
            logger.info("========== Inventory not persisted for Id : " + inventoryId + " and thus cannot update ==========");
            throw new InventoryException("Inventory does not exist for the given Id : " + inventoryId + " and thus cannot make update");
        }
        Inventory updatedInventory = inventoryRepository.save(inventory);
        logger.info("========== Updated inventory for Id " + updatedInventory.toString() + " ==========");
        return Optional.of(updatedInventory);
    }

    @Override
    @CacheEvict(value = "inventories_cache", allEntries = true)
    public Optional<Inventory> updateInventoryForDemand(Optional<Inventory> inventory, Integer demandQuantity) throws InventoryException {
        if (inventory.get().getQuantity() - demandQuantity < 0){
            logger.info("========== Sufficient inventory is not present and thus demand cannot be completed ==========");
            throw new InventoryException("Sufficient inventory is not present and thus demand cannot be completed");
        }
        inventory.get().setQuantity(inventory.get().getQuantity() - demandQuantity);
        Inventory updatedInventory = inventoryRepository.save(inventory.get());
        logger.info("========== Updated inventory for Id : " + updatedInventory.getId() + " demand ==========");
        return Optional.of(updatedInventory);
    }

    @Override
    @CacheEvict(value = "inventories_cache", allEntries = true)
    public void deleteInventoryById(Long inventoryId) throws InventoryException {
        Optional<Inventory> inventoryPersisted = inventoryRepository.findById(inventoryId);
        if(inventoryPersisted.isEmpty()){
            logger.info("========== Inventory not persisted for Id : " + inventoryId + " and thus cannot Delete ==========");
            throw new InventoryException("Inventory with given Id : -> " + inventoryId + " is not present thus cannot delete.");
        } else {
            logger.info("========== Deleted inventory for Id : " + inventoryId + " ==========");
            inventoryRepository.deleteById(inventoryId);
        }
    }

    public String welcomeToFalabella()
    {
        logger.info("========== API is in working condition ==========");
        return "Welcome to Order Management Service !!!!";
    }
}
