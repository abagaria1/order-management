package com.falabella.OrderManagementDemo.service;

import com.falabella.OrderManagementDemo.exception.InventoryException;
import com.falabella.OrderManagementDemo.models.Inventory;

import java.util.List;
import java.util.Optional;

public interface InventoryService {

    Inventory saveInventory(Inventory inventory) throws InventoryException;
    List<Inventory> fetchInventoryList() throws InventoryException;
    Optional<Inventory> updateInventory(Inventory inventory, Long inventoryId) throws InventoryException;
    Optional<Inventory> updateInventoryForDemand(Optional<Inventory> inventory, Integer demandQuantity) throws InventoryException;
    void deleteInventoryById(Long inventoryId) throws InventoryException;
    String welcomeToFalabella();
}
