package com.falabella.OrderManagementDemo.controller;

import com.falabella.OrderManagementDemo.exception.InventoryException;
import com.falabella.OrderManagementDemo.models.Demand;
import com.falabella.OrderManagementDemo.models.Inventory;

import org.springframework.beans.factory.annotation.Autowired;

import com.falabella.OrderManagementDemo.service.DemandService;
import com.falabella.OrderManagementDemo.service.InventoryService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/orders")
public class OrderManagementController {

    @Autowired
    InventoryService inventoryService;

    @Autowired
    DemandService demandService;

    @GetMapping("/welcome")
    public String getWelcomeMessage(){
        return inventoryService.welcomeToFalabella();
    }

    @GetMapping("/inventories")
    public List<Inventory> fetchInventoryList() throws InventoryException {
        return inventoryService.fetchInventoryList();
    }

    @PostMapping("/inventories")
    public Inventory saveInventory(@RequestBody Inventory inventory) throws InventoryException {
        return inventoryService.saveInventory(inventory);
    }

    @GetMapping("/demands")
    public List<Demand> fetchDemandList() throws InventoryException {
        return demandService.fetchAllDemands();
    }

    @PostMapping("/demands")
    public Optional<Demand> saveDemand(@RequestBody Demand demand) throws InventoryException{
        return demandService.saveDemand(demand);
    }

    @PutMapping("/inventories/{id}")
    public Optional<Inventory> updateInventory(@RequestBody Inventory inventory, @PathVariable("id") Long inventoryId) throws InventoryException {
        return inventoryService.updateInventory(inventory, inventoryId);
    }

    @DeleteMapping("/inventories/{id}")
    public String deleteInventoryById(@PathVariable("id") Long inventoryId) throws InventoryException {
        inventoryService.deleteInventoryById(inventoryId);
        return "Deleted Successfully";
    }

}
