package com.falabella.OrderManagementDemo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="inventory")
@SequenceGenerator(name="inventory_sequence", initialValue = 101, allocationSize = 100)
public class Inventory implements Serializable {

    private static final long serialVersionUID = 6581916216744133596L;

    public Inventory(){

    }

    public Inventory(Long productId, Integer quantity, String productLocation) {
        this.productId = productId;
        this.quantity = quantity;
        this.productLocation = productLocation;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "inventory_sequence")
    @Column(name="pid")
    private Long productId;

    @Column(name="quantity")
    private Integer quantity;

    @Column(name="location")
    private String productLocation;

    public Long getId() {
        return productId;
    }

    public void setId(Long productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getLocation() {
        return productLocation;
    }

    public void setLocation(String productLocation) {
        this.productLocation = productLocation;
    }

    @Override
    public String toString() {
        return "Inventory{" +
                "productId=" + productId +
                ", quantity=" + quantity +
                ", productLocation='" + productLocation + '\'' +
                '}';
    }
}
