package com.falabella.OrderManagementDemo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "demand")
@SequenceGenerator(name="inventory_sequence", initialValue = 1001, allocationSize = 100)
public class Demand implements Serializable {

    public Demand(){

    }

    public Demand(Long userId, Long productId, Integer quantity, String location) {
        this.userId = userId;
        this.productId = productId;
        this.quantity = quantity;
        this.location = location;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "demand_sequence")
    @Column(name="uid")
    private Long userId;

    @Column(name="pid")
    private Long productId;

    @Column(name="quantity")
    private Integer quantity;

    @Column(name="location")
    private String location;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Demand{" +
                "userId=" + userId +
                ", productId=" + productId +
                ", quantity=" + quantity +
                ", location='" + location + '\'' +
                '}';
    }
}
