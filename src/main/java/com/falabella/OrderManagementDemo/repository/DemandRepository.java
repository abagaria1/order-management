package com.falabella.OrderManagementDemo.repository;

import com.falabella.OrderManagementDemo.models.Demand;
import org.springframework.data.repository.CrudRepository;
public interface DemandRepository extends CrudRepository<Demand, Long> {

}

