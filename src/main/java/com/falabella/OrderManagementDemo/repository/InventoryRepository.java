package com.falabella.OrderManagementDemo.repository;

import com.falabella.OrderManagementDemo.models.Inventory;
import org.springframework.data.repository.CrudRepository;

public interface InventoryRepository extends CrudRepository<Inventory, Long> {
}

