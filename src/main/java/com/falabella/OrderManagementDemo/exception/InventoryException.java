package com.falabella.OrderManagementDemo.exception;

public class InventoryException extends Exception{

    public InventoryException(String message){
        super(message);
    }

}
